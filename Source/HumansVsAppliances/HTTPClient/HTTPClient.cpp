// Fill out your copyright notice in the Description page of Project Settings.


#include "HTTPClient.h"

#include "HttpManager.h"
#include "HttpRetrySystem.h"
#include "HttpModule.h"
#include "Interfaces/IHttpResponse.h"

#include<iostream>


FUserPoints::FUserPoints() {
	this->name = "";
	this->points = 0;
	this->range = 0;
}

FUserPoints::FUserPoints(FString name, int points) {
	this->name = name;
	this->points = points;
	this->range = 0;
}

FUserPoints::FUserPoints(FString name, int points, int range) {
	this->name = name;
	this->points = points;
	this->range = range;
}

FUserPoints::FUserPoints(FString name, FString newname, int range, int points) {
	this->name = name;
	this->points = points;
	this->range = range;
	this->newname = newname;
}

FUserPoints::FUserPoints(FString name, int points, FString option) {
	this->name = name;
	this->points = points;
	this->option = option;
}

HTTPClient::HTTPClient(){
}

HTTPClient::~HTTPClient(){
}


TArray<FUserPoints> ParseUsersArray(FString& js) {
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(js);
	TSharedPtr<FJsonObject> jsobj;
	FJsonSerializer::Deserialize(*Reader, jsobj);

	TArray<FUserPoints> res;

	TArray<FString> keys;
	jsobj.Get()->Values.GetKeys(keys);

	for (FString& k : keys) {
		int range = FCString::Atoi(*k);
		const TSharedPtr<FJsonObject>& inner = jsobj.Get()->GetObjectField(k);
		FUserPoints us = FUserPoints(inner.Get()->GetStringField("user"), inner.Get()->GetIntegerField("points"), range);
		res.Add(us);
	}

	return res;
}



FString GetHash(FString str) {
	return FMD5::HashAnsiString(*str).ToUpper();
}

void HTTPClient::GetTable() {
	
}

FString UsrToStr(const FUserPoints& user) {
	FString res = FString();
	res += "{\"user\":\"" + user.name + "\",";
	res += "\"points\":\"" + FString::FromInt(user.points) + "\",";
	if (!user.newname.IsEmpty())
		res += "\"newname\":\"" + user.newname + "\",";
	if (!user.option.IsEmpty())
		res += "\"option\":\"" + user.option + "\",";
	res += "\"hash\":\"" + 
		GetHash(user.name + "_" + FString::FromInt(user.points)) +
		"\"}";
	
	return res;
}

void HTTPClient::CreateUser(const FUserPoints& user){
	FHttpModule* Mod = &FHttpModule::Get();
	FHttpRequestRef req = Mod->CreateRequest();
	req.Get().SetURL("https://dmlsapi.ru/users/put");
	req.Get().SetVerb("PUT");
	req.Get().SetHeader("Content-Type", "application/json");
	req.Get().SetContentAsString(UsrToStr(user));
	req.Get().OnProcessRequestComplete().BindLambda(
		[=](FHttpRequestPtr req, FHttpResponsePtr res, bool bOk) {
			if (bOk && res.IsValid()) {
				CallbackCreated.Broadcast(res.Get()->GetResponseCode());
			}

		}
	);
	req.Get().ProcessRequest();
}

void HTTPClient::UpdateUser(const FUserPoints& user){
	FHttpModule* Mod = &FHttpModule::Get();
	FHttpRequestRef req = Mod->CreateRequest();
	req.Get().SetURL("https://dmlsapi.ru/users/update");
	req.Get().SetVerb("POST");
	req.Get().SetHeader("Content-Type", "application/json");
	req.Get().SetContentAsString(UsrToStr(user));
	req.Get().OnProcessRequestComplete().BindLambda(
		[=](FHttpRequestPtr req, FHttpResponsePtr res, bool bOk) {
			if (bOk && res.IsValid()) {
				CallbackUpdated.Broadcast(res.Get()->GetResponseCode());
			}

		}
	);
	req.Get().ProcessRequest();
}

void HTTPClient::GetUsers(FString name){
	FHttpModule* Mod = &FHttpModule::Get();
	FHttpRequestRef req = Mod->CreateRequest();
	req.Get().SetURL("https://dmlsapi.ru/users/get?top=top&name=" + name);
	req.Get().SetVerb("GET");

	req.Get().OnProcessRequestComplete().BindLambda(
		[=](FHttpRequestPtr req, FHttpResponsePtr res, bool bOk) {
			if (bOk && res.IsValid()) {
				FString s = res.Get()->GetContentAsString();
				FUserPointsArray arr{ ParseUsersArray(s) };

				CallbackGettedUsers.Broadcast(res.Get()->GetResponseCode(), arr);
			}

		}
	);
	req.Get().ProcessRequest();
}

