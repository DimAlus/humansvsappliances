// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "HTTPClient.generated.h"



USTRUCT(BlueprintType)
struct FUserPoints {
	GENERATED_BODY()
public:
	FUserPoints();
	FUserPoints(FString name, int points);
	FUserPoints(FString name, int points, int range);
	FUserPoints(FString name, FString newname, int range, int points);
	FUserPoints(FString name, int points, FString option);
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString option;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString newname;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int points;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int range;
};


USTRUCT(BlueprintType)
struct FUserPointsArray {
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FUserPoints> Users;
};
//
//typedef void (*CallbackInt)(int);
//typedef void (*CallbackUsrs)(int, const TArray<FUserPoints>&);

//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCallbackUpdatedSignature, int32, code);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCallbackCreatedSignature, int32, code);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FCallbackGettedUsersSignature, int32, code, const FUserPointsArray&, users);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCallbackInt, int32, code);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FCallbackIntUsers, int32, code, const FUserPointsArray&, users);


/**
 * 
 */
class HUMANSVSAPPLIANCES_API HTTPClient
{
public:
	HTTPClient();
	~HTTPClient();

	static void GetTable();
	void CreateUser(const FUserPoints& user);
	void UpdateUser(const FUserPoints& user);
	void GetUsers(FString name);

	/*void CallbackGettedUsers(int32 code, const FUserPointsArray& users);
	void CallbackUpdated(int32 code);
	void CallbackCreated(int32 code);*/
	FCallbackInt CallbackUpdated;
	FCallbackInt CallbackCreated;
	FCallbackIntUsers CallbackGettedUsers;
};


