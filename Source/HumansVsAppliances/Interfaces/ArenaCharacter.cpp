// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaCharacter.h"

#include "DrawDebugHelpers.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "HumansVsAppliances/ActorComponents/ArenaHealthActorComponent.h"
#include "HumansVsAppliances/EnemiesPawn/EnemyPawn.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AArenaCharacter::AArenaCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HealthComponent = CreateDefaultSubobject<UArenaHealthActorComponent>(TEXT("HealthComponent"));
	if (HealthComponent)
	{
		HealthComponent->OnHealthEmpty.AddDynamic(this, &AArenaCharacter::CharacterDeadAnim);
	}
}

// Called when the game starts or when spawned
void AArenaCharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AArenaCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AArenaCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


void AArenaCharacter::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (bIsJumping)
	{
		if (abs(FVector::DotProduct(Hit.Normal, FVector::UpVector)) > 0.5)
		{
			bIsJumping = false;
			
			TArray<AActor*> IgnoredActor;
			IgnoredActor.Add(this);
			UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
				1000,
				1000 * 0.2f,
				GetActorLocation(),
				0,
				GetJumpingDamageRadius(),
				5,
				NULL, IgnoredActor, GetJumpingWeapon(), nullptr);

			PlayWeaponEffect_BP(EWeaponType::Jumping);
		}

	}

	
	
	
}
AActor* AArenaCharacter::GetJumpingWeapon(){
	return nullptr;
}
float AArenaCharacter::GetJumpingDamageRadius(){
	return 0;
}

void AArenaCharacter::PlayWeaponEffect_BP_Implementation(EWeaponType CurEffect)
{
	// BP
}


void AArenaCharacter::CharacterDeadAnim()
{
	GetCharacterMovement()->MaxWalkSpeed = 0;
	// ignore Enemy
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel6, ECollisionResponse::ECR_Ignore);
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel6, ECollisionResponse::ECR_Ignore);
	// ignore Projectile
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
	// ignore Enemy
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
	if (bIsAlive && AnimDead)
	{
		GetWorldTimerManager().SetTimer(TimerHandle_PlayingDeathAnim,
										this,
										&AArenaCharacter::CharacterDead,
										PlayAnimMontage(AnimDead),
										false);
	}
	else if (bIsAlive)
		CharacterDead();
	bIsAlive = false;
}

void AArenaCharacter::CharacterDead()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_PlayingDeathAnim);
	OnDead.Broadcast();
}
void AArenaCharacter::StartTakeDamageAnimation() {
	if (bIsAlive)
	{
		PlayDamageSound_BP();
		PlayAnimMontage(AnimTakeDamage);
	}
}

float AArenaCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	
		
	return Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
}

void AArenaCharacter::Destroyed()
{
	OnDead.Clear();
	
	Super::Destroyed();
}

void AArenaCharacter::PlayDamageSound_BP_Implementation()
{
	// in BP
}
