// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "HumansVsAppliances/PlayerPawn/ArenaPlayerPawn.h"
#include "ArenaEnemyAIController.generated.h"

/**
 * 
 */
UCLASS()
class HUMANSVSAPPLIANCES_API AArenaEnemyAIController : public AAIController
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=ControlledPawn)
	AEnemyPawn* CurPawn = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Attack)
	bool bIsCanAttack;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Attack)
	float AttackDistance = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=MeleeEnemy)
	ESides CurSide;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=MeleeEnemy)
	float RadiusLength = 200;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=DistanceEnemy)
	bool bIsSafeZone;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=DistanceEnemy)
	float SafeDistance = 450.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Respawn)
	FTimerHandle TimerHandle_StartLogic;

	bool bIsStartStunVector = true;
	FVector StartStunVector;

	bool bIsGetRoundLeft = false;
	bool bIsGetRoundRight = false;

	FVector TargetRoundLeft;
	FVector TargetRoundRight;

	bool bIsWasUsedForStun = false;

	float ZHeight;
	
private:
	float TickToDealLogicTime = 0.01;
	float CurDeltaTime;
	float StunDeltaTime;
	float PastCurDeltaTime;
	float MoveDeltaTime;
	float MoveDeltaTimeSec;

	FVector ConstDeltaZ;

protected:
	void StartLogic();
	void StartMeleeLogic(float DeltaTime);
	void StartDistanceLogic(float DeltaTime);
	void StartBossLogic(float DeltaTime);

public:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void OnPossess(APawn* InPawn) override;

	virtual void Destroyed() override;
};
