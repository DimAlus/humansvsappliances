// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaEnemyAIController.h"
#include "EnemyPawn.h"
#include "NavigationPath.h"
#include "HumansVsAppliances/ActorComponents/ArenaHealthActorComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Runtime/NavigationSystem/Public/NavigationSystem.h"

/////////////////////////////
////////	Basic Overloaded
////////////////////////////

void AArenaEnemyAIController::BeginPlay()
{
	Super::BeginPlay();

	GetWorldTimerManager().SetTimer(TimerHandle_StartLogic,
			this,
			&AArenaEnemyAIController::StartLogic,
			TickToDealLogicTime,
			true);
}

void AArenaEnemyAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	CurSide = static_cast<ESides>(FMath::RandRange(0, 3));
	CurPawn = Cast<AEnemyPawn>(InPawn);

	if (CurPawn->CutEnemyType == EEnemyType::Router || CurPawn->CutEnemyType == EEnemyType::RobotVacuum)
	{
		AttackDistance = 100.0f;
	}
	else if (CurPawn->CutEnemyType == EEnemyType::Washer || CurPawn->CutEnemyType == EEnemyType::Fridge)
	{
		AttackDistance = 800.0f;
	}

	ZHeight = CurPawn->GetActorScale3D().Z*100;
	switch (CurPawn->CutEnemyType)
	{
	case EEnemyType::Router:
		ZHeight = 70;
		break;
	case EEnemyType::Washer:
		ZHeight = 60;
		break;
	case EEnemyType::RobotVacuum:
		ZHeight = 60;
		break;
	case EEnemyType::Fridge:
		ZHeight = 60;
		break;
	default: ;
	}
	CurPawn->SetActorLocation(CurPawn->GetActorLocation()+FVector(0, 0, ZHeight));
	ConstDeltaZ = FVector(0, 0, CurPawn->PlChar->GetMesh()->GetComponentScale().Z*3);
	 
}

void AArenaEnemyAIController::Destroyed()
{
	Super::Destroyed();
	GetWorldTimerManager().ClearTimer(TimerHandle_StartLogic);
}

void AArenaEnemyAIController::Tick(float DeltaTime)
{
	
	
	Super::Tick(DeltaTime);
}


/////////////////////////////
////////		Enemy Logics
////////////////////////////

void AArenaEnemyAIController::StartLogic()
{
	/*CurDeltaTime += DeltaTime;
	if (CurDeltaTime-PastCurDeltaTime >= TickToDealLogicTime)
	{*/
		PastCurDeltaTime = CurDeltaTime;
		if (CurPawn && CurPawn->PlController && CurPawn->PlChar)
		{
			if (CurPawn->bIsStan)
			{
				if (!bIsWasUsedForStun)
				{
					CurPawn->SetActorLocation(CurPawn->GetActorLocation()+FVector(0, 0, ZHeight));
					StartStunVector = CurPawn->GetActorLocation();
					bIsWasUsedForStun = true;
				}
					
				if (StunDeltaTime <= 0.3)
				{
					if (bIsStartStunVector)
					{
						//StartStunVector = CurPawn->GetActorLocation();
						bIsStartStunVector = false;
					}
						
					CurPawn->SetActorLocation(CurPawn->GetActorLocation()+FVector(0, 0, 3));
					CurPawn->SetActorRotation(CurPawn->GetActorRotation()+FRotator(0, 5, 0));
					StunDeltaTime += TickToDealLogicTime;
				}
				else if (StunDeltaTime <= 0.8)
				{
					CurPawn->SetActorLocation(CurPawn->GetActorLocation()+FVector(0, 0, -3));
					CurPawn->SetActorRotation(CurPawn->GetActorRotation()+FRotator(0, -5, 0));
					StunDeltaTime += TickToDealLogicTime;
				}
				else if (StunDeltaTime <= 1)
				{
					StunDeltaTime += TickToDealLogicTime;
				}
				else
				{
					CurPawn->SetActorLocation(StartStunVector - FVector(0, 0, ZHeight));
					CurPawn->bIsStan = false;
					bIsStartStunVector = true;
					StunDeltaTime = 0;
					bIsWasUsedForStun = false;
				}
			}
			else if (CurPawn->CutEnemyType == EEnemyType::Router || CurPawn->CutEnemyType == EEnemyType::RobotVacuum)
			{
				StartMeleeLogic(TickToDealLogicTime);
			}
			else if (CurPawn->CutEnemyType == EEnemyType::Washer)
			{
				StartDistanceLogic(TickToDealLogicTime);
			}
			else if (CurPawn->CutEnemyType == EEnemyType::Fridge)
			{
				StartBossLogic(TickToDealLogicTime);
			}
		}
	//}
}


void AArenaEnemyAIController::StartMeleeLogic(float DeltaTime)
{

	if(CurPawn->bIsAlive)
	{
		FVector DirEnemy = CurPawn->GetActorLocation();
		FVector DirPlayer = CurPawn->PlChar->GetActorLocation() - ConstDeltaZ;
		
		FVector CurDirection = CurPawn->PlChar->GetActorLocation() - ConstDeltaZ;
		if (bIsCanAttack || FVector::Dist(DirEnemy, DirPlayer) < AttackDistance)
		{
			CurPawn->AttackImpl();
		}
		else
		{
			switch (CurSide) {
			case ESides::Front:
				CurDirection += CurPawn->PlChar->GetActorForwardVector() * RadiusLength;
				break;
			case ESides::Back:
				CurDirection += CurPawn->PlChar->GetActorForwardVector() * -RadiusLength;
				break;
			case ESides::Left:
				CurDirection += CurPawn->PlChar->GetActorRightVector() * -RadiusLength;
				break;
			case ESides::Right:
				CurDirection += CurPawn->PlChar->GetActorRightVector() * RadiusLength;
				break;
			default:
				break;
			}
			
			FVector::Dist(DirEnemy, CurDirection) < AttackDistance ? bIsCanAttack = true : bIsCanAttack = false;
		
		}

		UNavigationPath* path = UNavigationSystemV1::GetCurrent(
			GetWorld())->FindPathToLocationSynchronously(GetWorld(),
												         GetPawn()->GetActorLocation(),
												         CurDirection,
												         NULL);




	MoveDeltaTime += DeltaTime;
	if (MoveDeltaTime >= 1)
		{
			FHitResult Hit;

			bIsGetRoundLeft = false;
			bIsGetRoundRight = false;
	 
			// We set up a line trace from our current location to a point 1000cm ahead of us
			FVector TraceStartLeft = (DirEnemy + CurPawn->GetActorRightVector()*50)*FVector(1, 1, 0.8);
			FVector TraceEndLeft = (DirEnemy + CurPawn->GetActorRightVector()*200)*FVector(1, 1, 0.8);

			FCollisionQueryParams QueryParams;
			//QueryParams.AddIgnoredActor(this);
	 
			GetWorld()->LineTraceSingleByChannel(Hit, TraceStartLeft, TraceEndLeft, ECollisionChannel::ECC_WorldDynamic, QueryParams);
			//DrawDebugLine(GetWorld(), TraceStartLeft, TraceEndLeft, Hit.bBlockingHit ? FColor::Blue : FColor::Red, false, 5.0f, 0, 10.0f);

			
			if (Hit.GetActor())
			{
				
				//UE_LOG(LogTemp, Log, TEXT("%s"), *Hit.GetActor()->GetName());
				if (Hit.GetActor()->GetName().Contains("BP_Router")||
					Hit.GetActor()->GetName().Contains("BP_RoboCleaner")||
					Hit.GetActor()->GetName().Contains("BP_Washer"))
				{
					bIsGetRoundLeft = true;
					TargetRoundLeft = DirEnemy - CurPawn->GetActorRightVector()*100;
				}
			}


			FVector TraceStartRight = (DirEnemy - CurPawn->GetActorRightVector()*50)*FVector(1, 1, 0.8);;
			FVector TraceEndRight = (DirEnemy - CurPawn->GetActorRightVector()*200)*FVector(1, 1, 0.8);
				
			GetWorld()->LineTraceSingleByChannel(Hit, TraceStartRight, TraceEndRight, ECollisionChannel::ECC_WorldDynamic, QueryParams);
			//DrawDebugLine(GetWorld(), TraceStartRight, TraceEndRight, Hit.bBlockingHit ? FColor::Blue : FColor::Red, false, 5.0f, 0, 10.0f);
				
			if (Hit.GetActor())
			{
				if (Hit.GetActor()->GetName().Contains("BP_Router")||
					Hit.GetActor()->GetName().Contains("BP_RoboCleaner")||
					Hit.GetActor()->GetName().Contains("BP_Washer"))
				{
					bIsGetRoundRight = true;
					TargetRoundRight = DirEnemy + CurPawn->GetActorRightVector()*100;
				}
			}
		
			MoveDeltaTime = 0;
			MoveDeltaTimeSec = 0;
		}

		MoveDeltaTimeSec += DeltaTime;

		if (bIsGetRoundLeft && MoveDeltaTimeSec < 0.1)
		{
			CurDirection = TargetRoundLeft;
		}
		else if (bIsGetRoundRight && MoveDeltaTimeSec < 0.1)
		{
			CurDirection = TargetRoundRight;
		}

		
		if (path && path->IsValid())
		{
			CurPawn->RotateTo(CurDirection);
			CurPawn->MoveTo(CurDirection);
		}
		else
		{
			CurPawn->RotateTo(DirPlayer);
			CurPawn->MoveTo(DirPlayer);
		}
	}

	


	
}

void AArenaEnemyAIController::StartDistanceLogic(float DeltaTime)
{
	if(CurPawn->bIsAlive)
	{
		FVector DirEnemy = CurPawn->GetActorLocation();
		FVector DirPlayer = CurPawn->PlChar->GetActorLocation() - ConstDeltaZ;
		
		float DistBetweenAct = FVector::Dist(DirPlayer, DirEnemy);

		DistBetweenAct < SafeDistance ? bIsSafeZone = false : bIsSafeZone = true;
		DistBetweenAct < AttackDistance ? bIsCanAttack = true : bIsCanAttack = false;

		if (!bIsSafeZone)
		{
			FVector NewVector = UKismetMathLibrary::GetDirectionUnitVector(CurPawn->PlChar->GetActorLocation(), CurPawn->GetActorLocation())*FVector(1, 1, 0);
			NewVector.Normalize();
			
			CurPawn->RotateTo(DirEnemy + NewVector);
			CurPawn->MoveTo(DirEnemy + NewVector);
		}
		else if (!bIsCanAttack)
		{
			MoveDeltaTime += DeltaTime;
			if (MoveDeltaTime >= 0.1) {
				CurPawn->RotateTo(DirPlayer);
				MoveDeltaTime = 0;
			}
			CurPawn->MoveTo(DirPlayer);
		}
		else if (bIsSafeZone == true && bIsCanAttack == true)
		{
			MoveDeltaTime += DeltaTime;
			if (MoveDeltaTime >= 0.1) {
				CurPawn->RotateTo(DirPlayer);
				MoveDeltaTime = 0;
			}
			CurPawn->AttackImpl();
		}
		
	}
}


void AArenaEnemyAIController::StartBossLogic(float DeltaTime)
{
	if(CurPawn->bIsAlive)
	{
		FVector DirEnemy = CurPawn->GetActorLocation();
		FVector DirPlayer = CurPawn->PlChar->GetActorLocation() - ConstDeltaZ;
		
		float DistBetweenAct = FVector::Dist(DirPlayer, DirEnemy);

		DistBetweenAct < SafeDistance ? bIsSafeZone = false : bIsSafeZone = true;
		DistBetweenAct < AttackDistance ? bIsCanAttack = true : bIsCanAttack = false;

		if (!bIsCanAttack)
		{
			MoveDeltaTime += DeltaTime;
			if (MoveDeltaTime >= 0.1) {
				CurPawn->RotateTo(DirPlayer);
				MoveDeltaTime = 0;
			}
			CurPawn->MoveTo(DirPlayer);
		}
		else if (bIsCanAttack == true)
		{
			MoveDeltaTime += DeltaTime;
			if (MoveDeltaTime >= 0.1) {
				CurPawn->RotateTo(DirPlayer);
				MoveDeltaTime = 0;
			}
			CurPawn->AttackImpl();
		}

		if (DistBetweenAct < 200)
		{
			CurPawn->PlChar->HealthComponent->ChangeHealthValue(-1);
		}
		
	}
}

